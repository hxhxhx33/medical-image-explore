MODEL_ARGS= \
	--resize=128,128,128 \
	--hidden_channel=6 \
	--patch_size=2,2,2 \
	--window_size=4,4,4

CHECKPOINT_DIR=local/checkpoints

train:
	python src/train.py \
		$(MODEL_ARGS) \
		--data_root=local/dataset/BraTS2021/TrainingData \
		--checkpoint_dir=$(CHECKPOINT_DIR) \
		--num_epochs=2 \

infer:
	python src/infer.py \
		$(MODEL_ARGS) \
		--data_root=local/dataset/BraTS2021/TrainingData \
		--checkpoint=$(CHECKPOINT_DIR)/model-final.pt \
		--output_dir=local/outputs
