import os
import json
import logging
import argparse
import typing
from functools import partial

import torch
import numpy as np
from monai.inferers.utils import sliding_window_inference
from monai.data.dataloader import DataLoader

import common.util as util
import common.argument as argument
import common.dataset as dataset
from model.model import Model

parser = argparse.ArgumentParser()
argument.add_common_args(parser=parser)
argument.add_model_args(parser=parser)
argument.add_input_args(parser=parser)
argument.add_inference_args(parser=parser)

def infer(
    inferer: typing.Callable[[torch.Tensor], torch.Tensor],
    loader: DataLoader,
    output_dir: str,
    device: torch.device,
):
    logging.info("Start inference")
    assert loader.batch_size == 1
  
    with torch.no_grad():
        for _, batch in enumerate(loader, 1):
            metadata = batch[f"{dataset.imageKey}_meta_dict"]

            file_path = metadata["filename_or_obj"][0]
            case_name = os.path.basename(os.path.dirname(file_path))
            logging.info(f"Infering {case_name}")

            image = batch[dataset.imageKey]
            image = image.to(device)

            prob = inferer(image)[0]

            # save result
            affine = metadata["original_affine"][0]
            save_path = os.path.join(output_dir, f"{case_name}_pred.nii.gz")
            dataset.save_label(
                prob=prob.detach().cpu().numpy(),
                affine=affine.detach().cpu().numpy(),
                save_path=save_path,
            )

def main() :
    args = parser.parse_args()
    argument.check_model_args(args)
    util.setup_logging(args.log_level.upper())

    # create non-existing directories
    output_dir = args.output_dir
    os.makedirs(output_dir, exist_ok=True)

    # data
    with open(args.fold_map) as f:
        fold_map = json.load(f)
    loader = dataset.create_data_loader_for_inference(
        data_root=args.data_root,
        fold_map=fold_map,
        fold=args.fold,
    )

    # device
    device = torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu')
    logging.info(f"Using device {device}")

    # model
    model = Model(
        patch_size=util.argparse_int_list(args.patch_size),
        window_size=util.argparse_int_list(args.window_size),
        stage_depths=util.argparse_int_list(args.swin_stage_depths),
        stage_num_heads=util.argparse_int_list(args.swin_stage_num_heads),
        input_channel=args.input_channel,
        hidden_channel=args.hidden_channel,
        output_channel=args.output_channel,
        mlp_ratio=args.swin_mlp_ratio,
    )
    model_dict = torch.load(args.checkpoint)["state_dict"] # type: ignore
    model.load_state_dict(model_dict)
    model.to(device)

    # create an inferer
    roi_size = util.argparse_int_list(args.roi_size)
    inferer = partial(
        sliding_window_inference,
        predictor=model,
        roi_size=roi_size,
        sw_batch_size=args.sliding_window_batch_size,
        overlap=args.sliding_window_overlap_ratio,
    )

    model.eval()
    infer(
        inferer=inferer, # type: ignore
        loader=loader,
        output_dir=output_dir,
        device=device,
    )

if __name__ == "__main__":
    main()
