import os
import json
import logging
import argparse
import typing

from monai.data.dataloader import DataLoader
from monai.metrics.metric import CumulativeIterationMetric

import common.util as util
import common.dataset as dataset
import common.argument as argument
import common.evaluate as evaluate

parser = argparse.ArgumentParser()
parser.add_argument("--prediction_dir", type=str, help="directory in which to-be-evaluated predictions are saved", required=True)
argument.add_common_args(parser=parser)
argument.add_evaluation_args(parser=parser)

def start(
    truth_loader: DataLoader,
    pred_lookup: typing.Dict[str, str],
    metric_func: CumulativeIterationMetric,
):
    assert truth_loader.batch_size == 1

    batch_metrics = util.AverageMeter()
    for _, batch in enumerate(truth_loader, 1):
        fpath = batch[f'{dataset.labelKey}_meta_dict']['filename_or_obj'][0]
        fname = os.path.basename(fpath)
        
        key = file_key(fname)
        pred_file = pred_lookup[key]

        pred = dataset.load_label(pred_file)
        pred = pred.unsqueeze(0)

        truth = batch[dataset.labelKey]
        metric, n = evaluate.calculate_batch_metric(
            truth=truth,
            probs=pred,
            metric_func=metric_func,
        )

        [dice_tc, dice_wt, dice_et] = metric
        logging.info(f"{fname}: TC(1+4) Dice: {dice_tc}, WT(1+2+4) Dice: {dice_wt}, ET(4) Dice: {dice_et}.")

        batch_metrics.update(metric, n)

    [dice_tc, dice_wt, dice_et] = batch_metrics.avg # type: ignore
    logging.info(f"Average: TC(1+4) Dice: {dice_tc}, WT(1+2+4) Dice: {dice_wt}, ET(4) Dice: {dice_et}.")

# Filename should be like XXXX_YYYY(_suffix).nii.gz
def file_key(fname: str) -> str:
    if fname.count("_") == 1:
        return fname.rstrip(".nii.gz")
    return fname.rsplit('_', 1)[0]

def main():
    # parse arguments
    args = parser.parse_args()
    util.setup_logging(args.log_level.upper())

    # create a loader to iterate ground truth
    with open(args.fold_map) as f:
        fold_map = json.load(f)
    loader = dataset.create_data_loader_for_evaluation(
        data_root=args.data_root,
        fold_map=fold_map,
        fold=args.fold,
    )

    # build a dict to look up predictions
    pdir = args.prediction_dir
    predictions = {}
    for f in os.listdir(pdir):
        fpath = os.path.join(pdir, f)
        if os.path.isfile(fpath):
            key = file_key(f)
            predictions[key] = fpath

    # create the metric function
    metric_func = evaluate.get_metric(args.metric)

    start(
        truth_loader=loader,
        pred_lookup=predictions,
        metric_func=metric_func,
    )

if __name__ == "__main__":
    main()
