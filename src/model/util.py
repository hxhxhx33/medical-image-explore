import math
import typing

import torch
import torch.nn.functional as F

def pad(
    x: torch.Tensor, # must have spatial dimensions at the tail
    units: typing.List[int], # must have length equal to the spatial dimensions
) -> typing.Tuple[torch.Tensor, typing.List[typing.Tuple[int, int]]]:
    [_, _, *dims] = x.size()
    assert len(dims) == len(units)

    pad: typing.List[typing.Tuple[int, int]] = []
    for d, u in zip(reversed(dims), reversed(units)):
        r = (u - d % u) % u
        l_pad = math.floor(r / 2)
        r_pad = math.ceil(r / 2)
        pad.append((l_pad, r_pad))
    padded = F.pad(x, pad=[v for p in pad for v in p])
    pad.reverse()

    return padded, pad

def unpad(
    x: torch.Tensor, # must have spatial dimensions at the tail
    pad: typing.List[typing.Tuple[int, int]], # must have length equal to the spatial dimensions
) -> torch.Tensor:
    [_, _, *dims] = x.size()
    assert len(dims) == len(pad)

    rs = [slice(None),slice(None)]
    for d, (l, r) in zip(dims, pad):
        rs.append(slice(l, d - r))

    return x[rs]

def cut_to_align(
    src: torch.Tensor, # must have spatial dimensions at the tail and each spatial dimension is not less than that of `dst`
    dst: torch.Tensor, # must have spatial dimensions at the tail
) -> torch.Tensor:
    [_, _, *src_dims] = src.size()
    [_, _, *dst_dims] = dst.size()
    assert len(src_dims) == len(dst_dims)

    rs = [slice(None),slice(None)]
    for [s, d] in zip(src_dims, dst_dims):
        assert s >= d
        r = s - d
        l_pad = math.floor(r / 2)
        r_pad = math.ceil(r / 2)
        rs.append(slice(l_pad, s - r_pad))

    return src[rs]