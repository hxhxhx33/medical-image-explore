import typing

import torch
import torch.nn as nn
from einops import rearrange # type: ignore

import model.util as model_util
from model.swin.block import Block

class Stage(nn.Module):
    def __init__(
        self,
        window_size: typing.List[int], # [w_D, w_H, w_W]
        window_shift: typing.List[int], # [d_D, d_H, d_W]
        num_channel: int,
        num_head: int,
        depth: int,
        mlp_ratio: int,
    ) -> None:
        super().__init__()

        self.window_size = window_size
        self.blocks = nn.ModuleList([
            Block(
                num_channel=num_channel,
                num_head=num_head, # all blocks of one stage have the same number of heads
                window_size=window_size,
                window_shift= [0] * len(window_size) if i % 2 == 0 else window_shift,
                mlp_ratio=mlp_ratio,
            ) for i in range(depth)
        ])
        self.merge = PatchMerging(
            num_channel=num_channel
        )
    def forward( # type: ignore
        self,
        x: torch.Tensor # (C, D, H, W)
    ) -> torch.Tensor:
        for block in self.blocks:
            x = block(x)
        x = self.merge(x)
        return x

class PatchMerging(nn.Module):
    def __init__(
        self,
        num_channel: int
    ) -> None:
        super().__init__()

        self.proj = nn.Linear(
            in_features=8 * num_channel,
            out_features=2 * num_channel,
            bias=False,
        )
    def forward( # type: ignore
        self,
        x: torch.Tensor # (C, D, H, W)
    ) -> torch.Tensor:
        x, _ = model_util.pad(x, [2] * (len(x.shape) - 2))

        # reshape since `nn.Linaer` acts on the last dimension.
        # TODO(hxu): generic
        x = rearrange(x, "n c d h w -> n d h w c")

        # TODO(hxu): generic
        x000 = x[:, 0::2, 0::2, 0::2, :]
        x001 = x[:, 0::2, 0::2, 1::2, :]
        x010 = x[:, 0::2, 1::2, 0::2, :]
        x100 = x[:, 1::2, 0::2, 0::2, :]
        x011 = x[:, 0::2, 1::2, 1::2, :]
        x101 = x[:, 1::2, 0::2, 1::2, :]
        x110 = x[:, 1::2, 1::2, 0::2, :]
        x111 = x[:, 1::2, 1::2, 1::2, :]
        x = torch.cat([x000, x001, x010, x100,x110, x011, x101, x111], -1)

        x = self.proj(x)
        x = rearrange(x, "n d h w c -> n c d h w")

        return x