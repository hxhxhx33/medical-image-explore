import typing
import itertools

import torch
import torch.nn as nn
import numpy as np
from einops import rearrange # type: ignore

from monai.networks.blocks.mlp import MLPBlock

import model.util as model_util
from model.swin.window_attention import WindowAttention

class Block(nn.Module):
    def __init__(
        self,
        num_channel: int,
        num_head: int,
        window_size: typing.List[int], # [w_D, w_H, w_W]
        window_shift: typing.List[int], # [d_D, d_H, d_W]
        mlp_ratio: int
    ) -> None:
        super().__init__()

        self.window_size = window_size
        self.window_shift = window_shift
        self.window_attention = WindowAttention(
            window_size=window_size,
            num_channel=num_channel,
            num_head=num_head,
        )

        self.norm1 = nn.LayerNorm(num_channel)
        self.norm2 = nn.LayerNorm(num_channel)
        self.mlp = MLPBlock(
            hidden_size=num_channel,
            mlp_dim=num_channel * mlp_ratio,
        )

    def forward( # type: ignore
        self,
        x: torch.Tensor # (C, D, H, W)
    ) -> torch.Tensor: # (C, D, H, W)
        x = x + self._forward_1(x)
        x = x + self._forward_2(x)
        return x

    def _forward_1(
        self,
        x: torch.Tensor # (C, D, H, W)
    ) -> torch.Tensor: # (C, D, H, W)
        # pad to make window partition possible
        x, pad = model_util.pad(x, self.window_size)
        [_, _, *spatial_dims] = x.size()

        # shift
        if np.any(self.window_shift): # type: ignore
            shifted_x = torch.roll(
                x,
                shifts=[-s for s in self.window_shift],
                dims=list(range(2, 2 + len(self.window_shift))),
            )
            mask = window_mask(
                spatial_dims=spatial_dims,
                window_size=self.window_size,
                window_shift=self.window_shift,
                device=x.device,
            )
        else:
            shifted_x = x
            mask = None

        # TODO(hxu): re-think the input shape
        shifted_x = rearrange(shifted_x, "n c d h w -> n d h w c")
        shifted_x = self.norm1(shifted_x)
        shifted_x = rearrange(shifted_x, "n d h w c -> n c d h w")

        windows = window_partition(shifted_x, self.window_size)
        windows = self.window_attention(windows, mask=mask)
        shifted_x = window_reunion(windows, self.window_size, spatial_dims)

        # unshift
        if np.any(self.window_shift): # type: ignore
            x = torch.roll(
                shifted_x,
                shifts=self.window_shift,
                dims=list(range(2, 2 + len(self.window_shift))),
            )
        else:
            x = shifted_x
            mask = None

        # unpad
        x = model_util.unpad(x, pad)

        return x

    def _forward_2(
        self,
        x: torch.Tensor # (C, D, H, W)
    ) -> torch.Tensor: # (C, D, H, W)
        # TODO(hxu): re-think the input shape
        x = rearrange(x, "n c d h w -> n d h w c")
        x = self.norm2(x)
        x = self.mlp(x)
        x = rearrange(x, "n d h w c -> n c d h w")
        return x

def window_partition(
    x: torch.Tensor, # (C, D, H, W)
    window_size: typing.List[int], # [w_D, w_H, w_W]
) -> torch.Tensor: # (D / w_W * H / w_H * W / w_W, w_W * w_H * w_W, C)
    # TODO(hxu): generic
    x = rearrange(
        x,
        # the (n d h w) dimensions are combined to a single batch dimension
        "n c (d wd) (h wh) (w ww) -> (n d h w) (wd wh ww) c",
        wd=window_size[0],
        wh=window_size[1],
        ww=window_size[2],
    )
    return x

def window_reunion(
    x: torch.Tensor,
    window_size: typing.List[int], # [w_D, w_H, w_W]
    spatial_dims: typing.List[int], # [D, H, W]
) -> torch.Tensor:
    assert len(spatial_dims) == len(window_size)
    for [d, s] in zip(spatial_dims, window_size):
        assert d % s == 0

    # TODO(hxu): generic
    x = rearrange(
        x,
        "(n d h w) (wd wh ww) c -> n c (d wd) (h wh) (w ww)",
        d=spatial_dims[0] // window_size[0],
        h=spatial_dims[1] // window_size[1],
        w=spatial_dims[2] // window_size[2],
        wd=window_size[0],
        wh=window_size[1],
        ww=window_size[2],
    )
    return x

def window_mask(
    spatial_dims: typing.List[int],
    window_size: typing.List[int],
    window_shift: typing.List[int],
    device: torch.device,
) -> torch.Tensor:
    assert len(window_size) == len(window_shift)
    assert len(spatial_dims) == len(window_size)
    for [d, s] in zip(spatial_dims, window_size):
        assert d % s == 0

    ranges = [
        (
            slice(-size),
            slice(-size, -shift),
            slice(-shift, None),
        ) for size, shift in zip(window_size, window_shift)
    ]
    
    label = 0
    cluster = torch.zeros(*spatial_dims, device=device)
    for idx in itertools.product(*ranges):
        cluster[idx] = label
        label += 1

    # TODO(hxu): generic
    windows = rearrange(
        cluster,
        "(d wd) (h wh) (w ww) -> (d h w) (wd wh ww)",
        wd=window_size[0],
        wh=window_size[1],
        ww=window_size[2],
    )
    mask = windows[:, :, None] - windows[:, None, :]
    mask = mask.masked_fill(mask != 0, -999.0).masked_fill(mask == 0, 0.0)
    return mask
