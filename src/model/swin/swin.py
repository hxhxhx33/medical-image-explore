import typing

import torch
import torch.nn as nn
from monai.networks.layers.factories import Conv
from monai.networks.layers.utils import get_norm_layer # type: ignore
from einops import rearrange # type: ignore

import model.util as model_util
from model.swin.stage import Stage

class Swin(nn.Module):
    def __init__(
        self,
        patch_size: typing.List[int], # [p_D, p_H, p_W]
        window_size: typing.List[int], # [w_D, w_H, w_W]
        stage_depths: typing.List[int],
        stage_num_heads: typing.List[int],
        input_channel: int, # C_i
        hidden_channel: int, # C_h
        mlp_ratio: int,
    ) -> None:
        super().__init__()
        assert len(stage_depths) == len(stage_num_heads)

        self.embed = PatchEmbedding(
            patch_size=patch_size,
            input_channel=input_channel,
            output_channel=hidden_channel,
        )
        self.stages = nn.ModuleList([
            Stage(
                window_size=window_size,
                window_shift=[s // 2 for s in window_size],
                num_channel=hidden_channel * (2 ** i),
                num_head=num_head,
                depth=depth,
                mlp_ratio=mlp_ratio,
            ) for i, [depth, num_head] in enumerate(zip(stage_depths, stage_num_heads))
        ])

    def forward( # type: ignore
        self,
        x: torch.Tensor # (C_i, D, H, W)
    ) -> typing.List[torch.Tensor]:
        outs: typing.List[torch.Tensor] = []
        
        x = self.embed(x) # (C_h, D / p_D, H / p_H, W / p_W)
        outs.append(x)

        for stage in self.stages:
            x = stage(x)
            outs.append(x)

        return outs

class PatchEmbedding(nn.Module):
    def __init__(
        self,
        patch_size: typing.List[int], # [p_D, p_H, p_W]
        input_channel: int, # C_in
        output_channel: int, # C_h
    ) -> None:
        super().__init__()

        self.patch_size = patch_size
        self.input_channel = input_channel
        self.output_channel = output_channel

        n_dim = len(patch_size)
        self.proj = Conv["conv", n_dim](
            in_channels=input_channel,
            out_channels=output_channel,
            kernel_size=patch_size,
            stride=patch_size,
        )

        # normalise along the output channel dimension,
        # i.e. the output (*, D / p_D, H / p_H, W / p_W) set will have mean 0 and variance 1.
        self.norm = get_norm_layer(name=("layer", {"normalized_shape": [output_channel]}))

    def forward( # type: ignore
        self,
        x: torch.Tensor  # (C_i, D, H, W)
    ) -> torch.Tensor:  # (C_h, D / p_D, H / p_H, W / p_W)
        [_, _, *dims] = x.size()
        assert len(dims) == len(self.patch_size)

        # pad to make patch partition possible
        x, _ = model_util.pad(x, self.patch_size)

        x = self.proj(x)

        # rearrange and normalize in a dimension-agnostic way
        [_, _, *dims] = x.size()
        dim_names = " ".join([f"d{i}" for i in range(len(dims))])
        dim_values = {}
        for i, d in enumerate(dims):
            dim_values[f"d{i}"] = d
        x = rearrange(x, f"n c {dim_names} -> n ({dim_names}) c")
        x = self.norm(x)
        x = rearrange(x, f"n ({dim_names}) c -> n c {dim_names}", **dim_values)

        return x
