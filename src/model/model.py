import typing

import torch
import torch.nn as nn
from monai.networks.layers.factories import Conv, Norm, Act

import model.util as model_util
from model.swin import Swin

class Model(nn.Module):
    def __init__(
        self,
        patch_size: typing.List[int],
        window_size: typing.List[int],
        stage_depths: typing.List[int],
        stage_num_heads: typing.List[int],
        input_channel: int,
        hidden_channel: int,
        output_channel: int,
        mlp_ratio: int,
    ) -> None:
        super().__init__()
        assert len(stage_depths) == len(stage_num_heads)

        num_dim = len(patch_size)
        num_stage = len(stage_depths)

        self.swin = Swin(
            patch_size=patch_size,
            window_size=window_size,
            input_channel=input_channel,
            hidden_channel=hidden_channel,
            stage_depths=stage_depths,
            stage_num_heads=stage_num_heads,
            mlp_ratio=mlp_ratio,
        )
        self.encoder = Encoder(
            in_channel=input_channel,
            out_channel=hidden_channel,
            num_dim=num_dim,
        )
        self.decoder = Decoder(
            in_channel=hidden_channel,
            out_channel=hidden_channel,
            num_dim=num_dim,
        )
        self.hidden_encoders = nn.ModuleList([
            Encoder(
                in_channel=hidden_channel * (2 ** i),
                out_channel=hidden_channel * (2 ** i),
                num_dim=num_dim,
            ) for i in range(num_stage + 1)
        ])
        self.hidden_decoders = nn.ModuleList([
            Decoder(
                in_channel=hidden_channel * (2 ** (i + 1)),
                out_channel=hidden_channel * (2 ** i),
                num_dim=num_dim,
            ) for i in range(num_stage)
        ])
        self.out = Conv["conv", num_dim](
            in_channels=hidden_channel,
            out_channels=output_channel,
            kernel_size=1,
            stride=1,
            padding=0,
        )

    def forward( # type: ignore TODO(hxu): figure out how to correctnly add type hit
        self,
        x: torch.Tensor
    ) -> torch.Tensor:
        hs = self.swin(x)
        es = [e(h) for e, h in zip(self.hidden_encoders, hs)]

        ds = [es[-1]]
        for decoder, e in zip(reversed(self.hidden_decoders), reversed(es[:-1])):
            d = decoder(ds[-1], e)
            ds.append(d)
        
        e = self.encoder(x)
        d = self.decoder(ds[-1], e)
        logits = self.out(d)
        probs = torch.sigmoid(logits)

        return probs

class Encoder(nn.Module):
    def __init__(
        self,
        in_channel: int,
        out_channel: int,
        num_dim: int, # TODO(hxu): generic
    ) -> None:
        super().__init__()

        self.conv1 = Conv["conv", num_dim](
            in_channels=in_channel,
            out_channels=out_channel,
            kernel_size=3,
            stride=1,
            padding=1,
        )
        self.norm1 = Norm["instance", num_dim](
            num_features=out_channel,
        )
        self.conv2 = Conv["conv", num_dim](
            in_channels=out_channel,
            out_channels=out_channel,
            kernel_size=3,
            stride=1,
            padding=1,
        )
        self.norm2 = Norm["instance", num_dim](
            num_features=out_channel,
        )

        self.act = Act["leakyrelu"](
            inplace=False, # TODO(hxu): figure out why the official code can set it to True.
            negative_slope=0.01,
        )
        self.proj = Conv["conv", num_dim](
            in_channels=in_channel,
            out_channels=out_channel,
            kernel_size=3,
            stride=1,
            padding=1,
        ) if in_channel != out_channel else nn.Identity()

    def forward( # type: ignore TODO(hxu)
        self,
        x: torch.Tensor # (C, D, H, W)
    ) -> torch.Tensor: # (C, D, H, W)
        residual = x

        x = self.conv1(x)
        x = self.norm1(x)
        x = self.act(x)
        x = self.conv2(x)
        x = self.norm2(x)
        x += self.proj(residual)
        x = self.act(x)

        return x

class Decoder(nn.Module):
    def __init__(
        self,
        in_channel: int,
        out_channel: int,
        num_dim: int, # TODO(hxu): generic
    ) -> None:
        super().__init__()

        self.convtrans = Conv["convtrans", num_dim](
            in_channels=in_channel,
            out_channels=out_channel,
            kernel_size=2,
            stride=2,
            padding=0,
        )
        self.proj = Encoder(
            in_channel=out_channel * 2,
            out_channel=out_channel,
            num_dim=num_dim,
        )

    def forward( # type: ignore TODO(hxu)
        self,
        x: torch.Tensor,
        a: torch.Tensor,
    ) -> torch.Tensor:
        x = self.convtrans(x)

        # Since we pad input during encoding, it may make more sense to cut input during deociding.
        x = model_util.cut_to_align(x, a)

        x = torch.cat([x, a], dim=1)
        x = self.proj(x)
        return x
