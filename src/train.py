import os
import json
import argparse
import logging
import typing
import time
from functools import partial

import torch
import torch.nn as nn
import torch.distributed as dist
import torch.multiprocessing as mp
from torch.optim.lr_scheduler import _LRScheduler
import numpy as np
from monai.inferers.utils import sliding_window_inference
from monai.transforms.post.array import AsDiscrete
from monai.data.dataloader import DataLoader
from monai.losses.dice import DiceLoss
from monai.metrics.metric import CumulativeIterationMetric

import wandb
from wandb.sdk.wandb_run import Run as Monitor

import common.dataset as dataset
import common.util as util
import common.argument as argument
import common.evaluate as evaluate
from model.model import Model

parser = argparse.ArgumentParser()
argument.add_common_args(parser=parser)
argument.add_model_args(parser=parser)
argument.add_input_args(parser=parser)
argument.add_training_args(parser=parser)
argument.add_evaluation_args(parser=parser)

def save_checkpoint(
    path: str,
    epoch: int,
    model: Model,
    optimizer: torch.optim.Optimizer,
    scheduler: _LRScheduler,
):
    save_dict = {
        "epoch": epoch,
        "state_dict": model.module.state_dict(),
        "optimizer": optimizer.state_dict(),
        "scheduler": scheduler.state_dict(),
    }
    torch.save(save_dict, path)  # type: ignore
    logging.info(f"Saved checkpoint for epoch {epoch} at {path}")

def train_epoch(
    model: Model,
    loader: DataLoader,
    optimizer: torch.optim.Optimizer,
    loss_func: nn.Module,
    device: torch.device,
    save_training_batches_dir: typing.Optional[str],
) -> float:
    batch_losses = util.AverageMeter()
    for idx, batch in enumerate(loader, 1):
        logging.info(f"Start batch {idx}/{len(loader)}")

        # debug
        if save_training_batches_dir is not None:
            dataset.save_training_batch(batch, save_training_batches_dir)
            logging.info(f"Saved batch to {save_training_batches_dir}")

        image, label = batch[dataset.imageKey], batch[dataset.labelKey]
        image, label = image.to(device), label.to(device)
        logging.debug(f"Batch files: {batch[f'{dataset.imageKey}_meta_dict']['filename_or_obj']}")
        logging.debug(f"Image shape: {image.shape}.")
        logging.debug(f"Label shape: {label.shape}.")
        logging.debug(f"Label summary: {torch.sum(label, [2, 3, 4])}.")

        if torch.max(label) == False:
            logging.warn("Meet a batch with all-empty label")
            # Uneven batches in DDP will block, see
            # https://discuss.pytorch.org/t/multiprocessing-barrier-blocks-all-processes/80345
            # https://github.com/pytorch/pytorch/issues/33148
            # Therefore, do not `continue`

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        prob = model(image) # (N, C_o, D, H, W)
        loss = loss_func(prob, label)
        loss.backward()
        optimizer.step()

        # collect loss
        batch_size = image.size()[0]
        batch_losses.update(loss.item(), batch_size) # type: ignore

    return batch_losses.avg  # type: ignore

def validate_epoch(
    inferer: typing.Callable[[torch.Tensor], torch.Tensor],
    loader: DataLoader,
    metric_func: CumulativeIterationMetric,
    device: torch.device,
):
    as_discrete = AsDiscrete(threshold=0.5)
    batch_metrics = util.AverageMeter()

    with torch.no_grad():
        for idx, batch in enumerate(loader, 1):
            logging.info(f"Start batch {idx}/{len(loader)}")

            image, label = batch[dataset.imageKey], batch[dataset.labelKey]
            image, label = image.to(device), label.to(device)

            probs = inferer(image)
            probs = as_discrete(probs)

            metric, n = evaluate.calculate_batch_metric(
                truth=label,
                probs=probs,
                metric_func=metric_func,
            )
            batch_metrics.update(metric, n)

    return batch_metrics.avg  # type: ignore

def train(
    rank: int,
    model: Model,
    inferer: typing.Callable[[torch.Tensor], torch.Tensor],
    train_loader: DataLoader,
    validate_loader: DataLoader,
    validate_interval: int,
    optimizer: torch.optim.Optimizer,
    loss_func: nn.Module,
    metric_func: CumulativeIterationMetric,
    max_epochs: int,
    start_epoch: int,
    checkpoint_dir: str,
    device: torch.device,
    monitor: typing.Optional[Monitor],
    save_training_batches_dir: typing.Optional[str],
    scheduler: _LRScheduler,
    save_checkpoint_interval: typing.Optional[int],
):
    logging.info("Start training")

    for eidx in range(start_epoch, max_epochs):
        train_loader.sampler.set_epoch(eidx)

        dist.barrier()
        logging.info(f"Start epoch {eidx + 1}/{max_epochs} with learning rate {scheduler.get_last_lr()}")

        model.train()
        train_loss = train_epoch(
            model=model,
            loader=train_loader,
            optimizer=optimizer,
            loss_func=loss_func,
            device=device,
            save_training_batches_dir=save_training_batches_dir,
        )
        logging.info(f"Finished epoch {eidx + 1}/{max_epochs}. Training loss: {train_loss}")

        if monitor is not None and rank == 0:
            monitor.log({"train_loss": train_loss})

        if (eidx + 1) % validate_interval == 0 and len(validate_loader) > 0:
            dist.barrier()
            logging.info("Start validating")

            model.eval()
            batch_metrics = validate_epoch(
                inferer=inferer,
                loader=validate_loader,
                metric_func=metric_func,
                device=device,
            )
            [dice_tc, dice_wt, dice_et] = batch_metrics # type: ignore
            logging.info(f"Validated epoch {eidx + 1}/{max_epochs}. TC Dice: {dice_tc}. WT Dice: {dice_wt}. ET Dice: {dice_et}")

        scheduler.step()

        if (rank == 0) and (save_checkpoint_interval is not None) and ((eidx + 1) % save_checkpoint_interval == 0) and ((eidx + 1) < max_epochs):
            save_path = os.path.join(checkpoint_dir, f"model-{eidx + 1}.pt")
            save_checkpoint(
                path=save_path,
                epoch=max_epochs,
                model=model,
                optimizer=optimizer,
                scheduler=scheduler,    
            )

    if rank == 0:
        save_path = os.path.join(checkpoint_dir, f"model-{max_epochs}.pt")
        save_checkpoint(
            path=save_path,
            epoch=max_epochs,
            model=model,
            optimizer=optimizer,
            scheduler=scheduler,    
        )

    logging.info("Finished training")

def start(
    gpu_index: int,
    args: argparse.Namespace,
    monitor: typing.Optional[Monitor],
):
    util.setup_logging(args.log_level.upper(), prefix=f"Worker-{gpu_index}")
    logging.info(f"Start training on GPU {gpu_index}")

    # distribtued data parallel
    # TODO(hxu): support more than one machines
    num_gpu = torch.cuda.device_count()
    dist.init_process_group(
        backend="nccl",
        world_size=num_gpu,
        rank=gpu_index,
    )

    # data
    with open(args.fold_map) as f:
        fold_map = json.load(f)
    roi_size = util.argparse_int_list(args.roi_size)
    [train_loader, validate_loader] = dataset.create_data_loader_for_training(
        data_root=args.data_root,
        train_batch_size=args.train_batch_size,
        roi_size=roi_size,
        fold_map=fold_map,
        fold=args.fold,
    )

    device = torch.device(f'cuda:{gpu_index}' if torch.cuda.is_available() else 'cpu')
    logging.info(f"Using device {device}")
    torch.backends.cudnn.benchmark = True
    torch.cuda.set_device(device)

    # model
    model = Model(
        patch_size=util.argparse_int_list(args.patch_size),
        window_size=util.argparse_int_list(args.window_size),
        stage_depths=util.argparse_int_list(args.swin_stage_depths),
        stage_num_heads=util.argparse_int_list(args.swin_stage_num_heads),
        input_channel=args.input_channel,
        hidden_channel=args.hidden_channel,
        output_channel=args.output_channel,
        mlp_ratio=args.swin_mlp_ratio,
    )
    model.to(device)

    # an inferer for validation
    inferer = partial(
        sliding_window_inference,
        predictor=model,
        roi_size=roi_size,
        sw_batch_size=args.sliding_window_batch_size,
        overlap=args.sliding_window_overlap_ratio,
    )

    # optimizer, loss and metric
    optimizer = torch.optim.AdamW(
        model.parameters(),
        lr=args.lr,
        weight_decay=args.weight_decay,
    )
    loss_func = DiceLoss(
        to_onehot_y=False,
        sigmoid=False,
    )
    metric_func = evaluate.get_metric(args.metric)
    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
        optimizer,
        T_max=args.max_epochs,
    )
    
    # checkpoint
    start_epoch = 0
    if args.checkpoint is not None:
        # load pre-trained checkpoint
        checkpoint = torch.load(
            args.checkpoint,
            map_location=device
        )
        start_epoch = checkpoint["epoch"]
        model.load_state_dict(checkpoint["state_dict"])
        optimizer.load_state_dict(checkpoint["optimizer"])
        scheduler.load_state_dict(checkpoint["scheduler"])
        logging.info(f"Loaded checkpoint {args.checkpoint} with trained epoch {start_epoch} and scheduler of last lr {scheduler.get_last_lr()}")

    checkpoint_dir=f"{args.checkpoint_dir}/{int(time.time())}"
    if gpu_index == 0:
        os.makedirs(checkpoint_dir, exist_ok=False)

    assert start_epoch + args.num_epochs <= args.max_epochs

    # DDP
    model = torch.nn.parallel.DistributedDataParallel(model, device_ids=[gpu_index])

    # start training
    train(
        rank=gpu_index,  
        model=model,
        inferer=inferer, # type: ignore
        train_loader=train_loader,
        validate_loader=validate_loader,
        validate_interval=args.validate_interval,
        optimizer=optimizer,
        scheduler=scheduler,
        loss_func=loss_func,
        metric_func=metric_func,
        start_epoch=start_epoch,
        max_epochs=start_epoch + args.num_epochs,
        checkpoint_dir=checkpoint_dir,
        device=device,
        monitor=monitor,
        save_training_batches_dir=args.save_training_batches_dir,
        save_checkpoint_interval=args.save_checkpoint_interval,
    )

def main():
    # parse arguments
    args = parser.parse_args()
    argument.check_model_args(args)
    util.setup_logging(args.log_level.upper())

    # monitor
    monitor = wandb.init(
        project=args.wandb_project,
        entity=args.wandb_entity,
        name=f"run-{int(time.time())}",
        config=vars(args),
    ) if args.wandb_project else None

    # create non-existing directories
    checkpoint_dir = args.checkpoint_dir
    os.makedirs(checkpoint_dir, exist_ok=True)

    # check GPU
    num_gpu = torch.cuda.device_count()
    logging.info(f"found {num_gpu} GPUs")

    # distribtued training
    mp.spawn(start, nprocs=num_gpu, args=(args, monitor,))

if __name__ == "__main__":
    main()
