import typing

import torch
from monai.data.utils import decollate_batch # type: ignore
from monai.utils.enums import MetricReduction
from monai.metrics.meandice import DiceMetric
from monai.metrics.metric import CumulativeIterationMetric

def get_metric(name: str) -> CumulativeIterationMetric:
    if name == "dice":
        return DiceMetric(
            include_background=True,
            reduction=MetricReduction.MEAN_BATCH,
            get_not_nans=True,
        )
    raise Exception(f"unrecognised metric name {name}")

T = typing.Sequence[torch.Tensor]
def calculate_batch_metric(
    truth: torch.Tensor,
    probs: torch.Tensor,
    metric_func: CumulativeIterationMetric,
) -> typing.Tuple[float, float]:
    truth_list = typing.cast(T, decollate_batch(truth))
    probs_list = typing.cast(T, decollate_batch(probs))

    metric_func(y_pred=probs_list, y=truth_list)
    metric, not_nans = typing.cast(T, metric_func.aggregate())

    return metric.cpu().numpy(), not_nans.cpu().numpy()
