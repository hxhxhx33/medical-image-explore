import os
import typing
import logging

import numpy as np

def setup_logging(level: str, prefix: str = ""):
    numeric_level = getattr(logging, level, None)
    assert isinstance(numeric_level, int)

    components = ["%(asctime)s.%(msecs)03d", "%(levelname)s"]
    if prefix != "":
        components.append(prefix)
    components.append("%(message)s")

    logging.basicConfig(
        level=numeric_level,
        datefmt='%Y-%m-%d %H:%M:%S',
        format=" ".join(components),
    )

def list_subfolders(root: str) -> typing.List[str]:
    subs: typing.List[str] = []
    for r in os.listdir(root):
        d = os.path.join(root, r)
        if os.path.isdir(d):
            subs.append(r)
    subs.sort()
    return subs

def argparse_int_list(liststr: str) -> typing.List[int]:
    return [int(v) for v in liststr.split(',')]

# TODO(hxu): type this class
class AverageMeter(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n = 1): # type: ignore
        self.val = val  # type: ignore
        self.sum += val * n  # type: ignore
        self.count += n
        self.avg = np.where(self.count > 0, self.sum / self.count, self.sum) # type: ignore
