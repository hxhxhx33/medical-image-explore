import argparse
import logging
import sys

import common.util as util

def add_common_args(parser: argparse.ArgumentParser):
    parser.add_argument("--log_level", type=str, default="INFO", help="log level")
    parser.add_argument("--data_root", type=str, help="dataset root directory")
    parser.add_argument("--fold_map", type=str, help="path to the fold map json file", required=True)
    parser.add_argument("--fold", type=int, help="fold index used as validation")

def add_model_args(parser: argparse.ArgumentParser):
    parser.add_argument("--patch_size", type=str, default="2,2,2", help="comma-separated patch size for each dimension")
    parser.add_argument("--window_size", type=str, default="7,7,7", help="comma-separated window size for each dimension")
    parser.add_argument("--swin_stage_depths", type=str, default="2,2,2,2", help="comma-separated depth for each swin stage")
    parser.add_argument("--swin_stage_num_heads", type=str, default="3,6,12,24", help="comma-separated number of heads for each swin stage")
    parser.add_argument("--swin_mlp_ratio", type=int, default=4, help="ratio of mlp dimensions to channel size in swin block")
    parser.add_argument("--input_channel", type=int, default=4, help="number of input channel")
    parser.add_argument("--hidden_channel", type=int, default=48, help="number of hidden channel")
    parser.add_argument("--output_channel", type=int, default=3, help="number of output channel")

def add_input_args(parser: argparse.ArgumentParser):
    parser.add_argument("--roi_size", type=str, default="128,128,128", help="comma-separated RoI size to crop the original images for training and inference")
    parser.add_argument("--sliding_window_batch_size", type=int, default=1, help="batch size for sliding window inference")
    parser.add_argument("--sliding_window_overlap_ratio", type=float, default=0.5, help="overlap ratio for sliding window inference")

def add_training_args(parser: argparse.ArgumentParser):
    # dataset
    parser.add_argument("--train_batch_size", type=int, default=1, help="batch size for training")

    # chackpoint
    parser.add_argument("--checkpoint", type=str, help="path to a pre-trained checkpoint")

    # train
    parser.add_argument("--max_epochs", type=int, default=800, help="maximum number of epochs to train")
    parser.add_argument("--num_epochs", type=int, default=100, help="number of epochs to train")
    parser.add_argument("--lr", type=float, default=1e-4, help="optimizer learning rate")
    parser.add_argument("--weight_decay", type=float, default=1e-5, help="optimizer weight decay")
    parser.add_argument("--save_checkpoint_interval", type=int, help="number of training epoch after which to save checkpoint")
    parser.add_argument("--validate_interval", type=int, default=100, help="number of training epoch after which to run validation")
    parser.add_argument("--checkpoint_dir", type=str, help="directory to save checkpoint", required=True)

    # wandb
    parser.add_argument("--wandb_project", type=str, help="wandb project")
    parser.add_argument("--wandb_entity", type=str, help="wandb entity")

    # debug
    parser.add_argument("--save_training_batches_dir", type=str, help="directory to set training batches for debugging")

def add_evaluation_args(parser: argparse.ArgumentParser):
    parser.add_argument("--metric", default="dice", type=str, help="name of the metric used for evaluation")

def add_inference_args(parser: argparse.ArgumentParser):
    # checkpoint
    parser.add_argument("--checkpoint", type=str, help="path to the checkpoint", required=True)
 
    # output
    parser.add_argument("--output_dir", type=str, help="directory to save outputs")

def check_model_args(args: argparse.Namespace):
    ok = True

    hidden_channel = args.hidden_channel
    swin_stage_num_heads = util.argparse_int_list(args.swin_stage_num_heads)
    for i, h in enumerate(swin_stage_num_heads):
        v = hidden_channel * (2 ** i)
        if v % h != 0:
            logging.fatal(f"the {i}-th `swin_stage_num_head`(={h}) must divide `hidden_channel`(={hidden_channel}) * 2^{i}(={v})")
            ok = False


    if not ok:
        sys.exit(1)
