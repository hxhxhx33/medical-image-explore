import os
import typing
import logging
from math import ceil

import numpy as np
import torch
from torch.utils.data.distributed import DistributedSampler

from monai.data.dataset import Dataset
from monai.data.dataloader import DataLoader
from monai.transforms.transform import MapTransform
from monai.transforms.compose import Compose
from monai.transforms.io.dictionary import LoadImaged
from monai.transforms.intensity.dictionary import NormalizeIntensityd, RandScaleIntensityd, RandShiftIntensityd
from monai.transforms.utility.dictionary import ConvertToMultiChannelBasedOnBratsClassesd, ToTensord
from monai.transforms.croppad.dictionary import CropForegroundd, RandSpatialCropd
from monai.transforms.spatial.dictionary import RandFlipd
import nibabel as nib # type: ignore

import common.util as util

def create_data_loader_for_training(
    data_root: str,
    train_batch_size: int,
    roi_size: typing.Sequence[int],
    fold_map: typing.Dict[str, int],
    fold: int
) -> typing.Tuple[DataLoader, DataLoader]:
    data_dirs = util.list_subfolders(data_root)

    train_dirs = []
    validate_dirs = []
    for data_dir in data_dirs:
        if data_dir not in fold_map:
            logging.info(f"ignored un-folded data {data_dir}")
            continue
        curr_fold = fold_map[data_dir]
        if curr_fold == fold:
            validate_dirs.append(data_dir)
        else:
            train_dirs.append(data_dir)
    logging.info(f"Using fold {fold} for validation with {len(train_dirs)} training data and {len(validate_dirs)} validation data")

    common_trans = [
        *_common_image_transformations(),
        *_common_label_transformations(),
    ]

    # train
    train_transform = Compose([
        *common_trans,

        # crop
        CropForegroundd(keys=[imageKey, labelKey], source_key=imageKey, k_divisible=roi_size),
        RandSpatialCropd(keys=[imageKey, labelKey], roi_size=roi_size, random_size=False, random_center=True),

        # augment
        RandFlipd(keys=[imageKey, labelKey], prob=0.5, spatial_axis=0),
        RandFlipd(keys=[imageKey, labelKey], prob=0.5, spatial_axis=1),
        RandFlipd(keys=[imageKey, labelKey], prob=0.5, spatial_axis=2),
        RandScaleIntensityd(keys=imageKey, factors=0.1, prob=1.0),
        RandShiftIntensityd(keys=imageKey, offsets=0.1, prob=1.0),

        ToTensord(keys=[imageKey, labelKey]),
    ])
    train_data = [_make_data_dict(data_root, dir) for dir in train_dirs]
    train_dataset = Dataset(data=train_data, transform=train_transform)
    train_sampler = DistributedSampler(train_dataset, shuffle=True)
    train_loader = DataLoader(
        train_dataset,
        batch_size=train_batch_size,
        sampler=train_sampler,
        pin_memory=True,
        num_workers=8,
    )
    logging.info(f"Number of training batches: {len(train_loader)}")

    # validate
    validate_transform = Compose([
        *common_trans,
        ToTensord(keys=[imageKey, labelKey]),
    ])
    validate_data = [_make_data_dict(data_root, dir) for dir in validate_dirs]
    validate_dataset = Dataset(data=validate_data, transform=validate_transform)
    validate_sampler = DistributedSampler(validate_dataset, shuffle=False)
    validate_loader = DataLoader(
        validate_dataset,
        batch_size=1,
        sampler=validate_sampler,
        pin_memory=True,
        num_workers=8,
    )
    logging.info(f"Number of validation batches: {len(validate_loader)}")

    return (train_loader, validate_loader)

def create_data_loader_for_inference(
    data_root: str,
    fold_map: typing.Dict[str, int],
    fold: int
) -> DataLoader:
    data_dirs = util.list_subfolders(data_root)
    dirs = []
    for data_dir in data_dirs:
        if data_dir not in fold_map:
            logging.info(f"ignored un-folded data {data_dir}")
            continue
        curr_fold = fold_map[data_dir]
        if curr_fold == fold:
            dirs.append(data_dir)
    logging.info(f"Using fold {fold} for inference with {len(dirs)} data")

    trans = _common_image_transformations()

    transform = Compose([
        *trans,
        ToTensord(keys=[imageKey]),
    ])
    data = [_make_data_dict(data_root, dir) for dir in dirs]
    dataset = Dataset(data=data, transform=transform)
    loader = DataLoader(dataset, batch_size=1, shuffle=False,)
    return loader

def create_data_loader_for_evaluation(
    data_root: str,
    fold_map: typing.Dict[str, int],
    fold: int
) -> DataLoader:
    data_dirs = util.list_subfolders(data_root)
    dirs = []
    for data_dir in data_dirs:
        if data_dir not in fold_map:
            logging.info(f"ignored un-folded data {data_dir}")
            continue
        curr_fold = fold_map[data_dir]
        if curr_fold == fold:
            dirs.append(data_dir)
    logging.info(f"Using fold {fold} for evaluation with {len(dirs)} data")

    trans = _common_label_transformations()

    transform = Compose([
        *trans,
        ToTensord(keys=[labelKey]),
    ])
    data = [_make_data_dict(data_root, dir) for dir in dirs]
    dataset = Dataset(data=data, transform=transform)
    loader = DataLoader(dataset, batch_size=1, shuffle=False,)
    return loader

def load_label(fpath: str) -> torch.Tensor:
    trans = _common_label_transformations()
    transform = Compose([
        *trans,
        ToTensord(keys=[labelKey]),
    ])
    loaded = transform({ labelKey: fpath })
    return loaded[labelKey]

def save_nifti(
    data: np.array,
    affine: np.array,
    save_path: str,
):
    pred = nib.Nifti1Image(data, affine)
    nib.save(pred, save_path) # type: ignore

def save_label(
    prob: np.array,
    affine: np.array,
    save_path: str,
):
    seg = (prob > 0.5).astype(np.int8)
    seg_out = np.zeros((seg.shape[1], seg.shape[2], seg.shape[3]))
    seg_out[seg[1] == 1] = 2
    seg_out[seg[0] == 1] = 1
    seg_out[seg[2] == 1] = 4
    seg_out = seg_out.astype(np.uint8)

    save_nifti(
        data=seg_out,
        affine=affine,
        save_path=save_path,
    )

def save_training_batch(
    batch: any, # TODO(hxu): set type
    batch_probs: typing.Optional[np.array],
    dir: str
):
    image, label = batch[imageKey], batch[labelKey]
    image = image.detach().cpu().numpy()
    label = label.detach().cpu().numpy()

    channel_names = ["flair", "t1ce", "t1", "t2"]

    batch_size = image.shape[0]
    for sidx in range(batch_size):
        metadata = batch[f"{imageKey}_meta_dict"]
        file_path = metadata["filename_or_obj"][sidx]
        case_name = os.path.basename(os.path.dirname(file_path))
        affine = metadata["original_affine"][sidx].detach().cpu().numpy()    

        save_dir = f"{dir}/{case_name}"
        os.makedirs(save_dir, exist_ok=True)

        for cidx, channel in enumerate(channel_names):
            save_nifti(
                data=image[sidx][cidx],
                affine=affine,
                save_path=f"{save_dir}/{case_name}_{channel}.nii.gz",
            )
        save_label(
            prob=label[sidx],
            affine=affine,
            save_path=f"{save_dir}/{case_name}_seg.nii.gz",
        )
        if batch_probs is not None:
            save_label(
                prob=batch_probs[sidx],
                affine=affine,
                save_path=f"{save_dir}/{case_name}_pred.nii.gz",
            )

# must be the name of two fields of `_DataDict`
[imageKey, labelKey] = ["image", "label"]

class _DataDict(typing.TypedDict):
    image: typing.Tuple[str, str, str, str]
    label: str

def _make_data_dict(root: str, dir: str, has_label: bool = True) -> _DataDict:
    return {
        "image": (
            f"{root}/{dir}/{dir}_flair.nii.gz",
            f"{root}/{dir}/{dir}_t1ce.nii.gz",
            f"{root}/{dir}/{dir}_t1.nii.gz",
            f"{root}/{dir}/{dir}_t2.nii.gz",
        ),
        "label": f"{root}/{dir}/{dir}_seg.nii.gz" if has_label else "",
    }

def _common_image_transformations() -> typing.List[MapTransform]:
    trans: typing.List[MapTransform] = [
        LoadImaged(keys=[imageKey]),
        NormalizeIntensityd(keys=imageKey, nonzero=True, channel_wise=True),
    ]
    return trans

def _common_label_transformations() -> typing.List[MapTransform]:
    trans: typing.List[MapTransform] = [
        LoadImaged(keys=[labelKey]),
        ConvertToMultiChannelBasedOnBratsClassesd(keys=labelKey),
    ]
    return trans
